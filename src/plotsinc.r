library(plotly)
library(ggplot2)
#png('sincfuncs.png')

values<-read.csv('sincfuncs.csv',header=TRUE,sep=';')
colnames(values)<- c("x","Sinc_x","Sinc_Norm_x")
# plot(values$x,values$Sinc_x)
# points(values$x,values$Sinc_Norm_x)
# ggplot(values,aes(x=x, y=Sinc_x)) + geom_line(aes(color='red'))

p = ggplot() +
  geom_line(data = values , aes(x = x, y = Sinc_x), color = "blue") +
  geom_line(data = values, aes(x = x, y = Sinc_Norm_x), color = "red") +
  xlab('x') +
  ylab('Sinc(x), SincNorm(x)') +
  ggtitle('Sinc and Normalized Sinc functions') +
  theme(plot.title = element_text(color="red", size=20, face="bold.italic"))

#p
options(viewer=NULL)
ggplotly(p)
#dev.off()
