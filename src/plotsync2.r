# Multiple plot function
#
# ggplot objects can be passed in ..., or to plotlist (as a list of ggplot objects)
# - cols:   Number of columns in layout
# - layout: A matrix specifying the layout. If present, 'cols' is ignored.
#
# If the layout is something like matrix(c(1,2,3,3), nrow=2, byrow=TRUE),
# then plot 1 will go in the upper left, 2 will go in the upper right, and
# 3 will go all the way across the bottom.
#
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  library(grid)

  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)

  numPlots = length(plots)

  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                    ncol = cols, nrow = ceiling(numPlots/cols))
  }

 if (numPlots==1) {
    print(plots[[1]])

  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))

    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))

      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}
library(ggplot2)

png('sincfuncs2.png')
values<-read.csv('sincfuncs.csv',header=TRUE,sep=';')
colnames(values)<- c("x","Sinc_x","Sinc_Norm_x")
plots<-list()

p1<-ggplot() +
  geom_line(data = values , aes(x = x, y = Sinc_x), color = "blue") +
  xlab('x') +
  ylab('Sinc(x), SincNorm(x)') +
  ggtitle('Sinc function') +
  theme(plot.title = element_text(color="red", size=20, face="bold.italic"))

p2<-ggplot() +
  geom_line(data = values, aes(x = x, y = Sinc_Norm_x), color = "red") +
  xlab('x') +
  ylab('SincNorm(x)') +
  ggtitle('Normalized Sinc function') +
  theme(plot.title = element_text(color="red", size=20, face="bold.italic"))

plots[[1]]<-p1
plots[[2]]<-p2

multiplot(plotlist=plots,cols=1)
dev.off()
