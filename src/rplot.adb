with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;
with Ada.Float_Text_Io; use Ada.Float_Text_io ;
with Ada.Numerics.Elementary_Functions ; use Ada.Numerics.Elementary_Functions ;

with Ada.Command_Line ; use Ada.Command_Line ;
procedure Rplot is
   argc : Integer := Argument_Count ;
   freq : float := 1.0 ;             -- Sinusoid frequency
   sfreq : Integer := 256 ;          -- Sampling frequency 
   time : Integer := 1;              -- seconds

   procedure generate_sinusoid is
      period : float := 1.0 / float(sfreq) ;
      x : float := 0.0 ;
      sinx : float ;
   begin
       Put_Line ("x ; sincubed ; sinxcubed");
       for samp in 1..sfreq*time
       loop
          sinx := Sin(2.0*Ada.Numerics.PI*x*freq) ;
          sinx := sinx * sinx * sinx ;
          Put(x) ; Put(" ; ") ; Put(sinx) ; 
          sinx := Sin(2.0*Ada.Numerics.PI*x*x*x*freq) ;
          Put(" ; ") ; Put(sinx) ;
          New_Line ;
          x := x + period ;
       end loop ;
   end generate_sinusoid;
   procedure simplex is
   begin 
      if argc >= 1
      then
         freq := float'value( argument(1) );
         if argc >= 2
         then
            sfreq := Integer'value( argument(2) );
            if argc >= 3
            then
               time := Integer'value( argument(3) );
               end if ;
         end if ;
      end if ;
      generate_sinusoid ;
   end simplex;

   procedure sincx_functions is
      num_points : constant Integer := 100 ;
      xstart : float := 0.001 ;
      xdelta : float := 0.1 ;
      x : float := xstart ;
      sincx, sincnormx : float ;
   begin
      for i in 1..num_points
      loop
         sincx := Sin(x) / x ;
         sincnormx := Sin(Ada.Numerics.pi * x) / (Ada.Numerics.pi * x) ;
         Put(x) ; Put(" ; ");
         Put(sincx) ; Put(" ; ");
         Put(sincnormx) ;
         New_Line ;
         x := x + xdelta ;
      end loop ;
   end sincx_functions;

   procedure lissajou is
      lissfile : File_Type ;
      A , B : float := 1.0 ;
      phase : float := Ada.Numerics.Pi/2.0 ;
      lca, lcb : float := 1.0 ;
      procedure generate_point( t, lca, lcb : float ) is
         x , y : float ;
      begin
         x := A * Sin( lca*t + phase );
         y := B * Sin( lcb*t );
         Put(t) ; Put(" ; "); Put(x) ; Put( " ; "); Put(y) ; New_Line;
      end generate_point;
   begin
      if Argument_Count >= 1
      then
         A := float'Value(Argument(1));
         if Argument_Count >= 2
         then
            B := float'Value(Argument(2));
         end if ;
      end if ;
      Create(lissfile,Out_File,"lissajou.csv") ;
      Set_Output(lissfile);
      for ratio in 1..8
      loop
        lcb := lca * float(ratio) ;
        for i in 1..100
         loop
            generate_point(0.01*float(i),lca,lcb);
            --generate_point(1.0/float(i*100),lca,lcb) ;
         end loop ;
      end loop ;
      close(lissfile);
   end lissajou;
begin
  lissajou ;
end Rplot;
